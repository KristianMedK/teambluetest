﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TeamBlueTest.Entities;

namespace TeamBlueTest.DB
{
    public class MyDbContext : DbContext
    {
        public MyDbContext(DbContextOptions<MyDbContext> options)
           : base(options)
        {
            this.Database.EnsureCreated();
        }

        public DbSet<WordsEntity> Words { get; set; }

        public DbSet<WatchlistWordsEntity> WatchList { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<WordsEntity>()
                .HasIndex(u => u.Word)
                .IsUnique();

            builder.Entity<WatchlistWordsEntity>()
               .HasIndex(w => w.Word)
               .IsUnique();
        }
    }
}
