﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EFCore.BulkExtensions;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using TeamBlueTest.DB;
using TeamBlueTest.Entities;
using TeamBlueTest.Models;

namespace TeamBlueTest.Controllers
{
    [Route("{controller}")]
    public class ApiController : Controller
    {

        MyDbContext _db;
        public ApiController(MyDbContext db)
        {
            _db = db;
        }

        [Route("Text")]
        [HttpPost]
        public async Task<IActionResult> Text([FromBody]TextRequestModel model)
        {
            var sw = Stopwatch.StartNew();
            var uniqueWords = new HashSet<string>();

            if (String.IsNullOrEmpty(model.Text))
                return BadRequest("Textvalue must have a value");

            var punctuation = model.Text.Where(Char.IsPunctuation).Distinct().ToArray();
            var words = model.Text.Split().Select(x => x.Trim(punctuation)); //get rid or . , etc. 

            Log.Logger.Information("part1 {@ms}", sw.ElapsedMilliseconds);
            foreach (var w in words)
                uniqueWords.Add(w.ToLower());

            var existingWords = _db.Words.Where(x => uniqueWords.Contains(x.Word)).Select(x => x.Word);
            var watchlistWords = _db.WatchList.Where(x => uniqueWords.Contains(x.Word)).Select(x => x.Word).ToList();
            var newWords = uniqueWords.Where(x => !existingWords.Contains(x));

            Log.Logger.Information("part2 {@ms}", sw.ElapsedMilliseconds);
            var entityList = new List<WordsEntity>();
            foreach (var uw in newWords)
                entityList.Add(new WordsEntity() { Word = uw });

            await _db.BulkInsertAsync<WordsEntity>(entityList);
            Log.Logger.Information("part3 {@ms}", sw.ElapsedMilliseconds);

            var response = new TextResponseModel()
            {
                distinctUniqueWords = entityList.Count(),
                WatchlistWords = watchlistWords
            };

            return Json(response);
        }

        [Route("Sampledata")]
        [HttpPost]
        public async Task<IActionResult> AddSampleData()
        {
            var text = System.IO.File.ReadAllText("SampleData.txt");

            var punctuation = text.Where(Char.IsPunctuation).Distinct().ToArray();
            var words = text.Split().Select(x => x.Trim(punctuation));

            var uniqueWords = new HashSet<string>();
            foreach (var w in words)
            {
                uniqueWords.Add(w.ToLower());
            }

            var entityList = new List<WordsEntity>();
            foreach (var uw in uniqueWords)
            {
                entityList.Add(new WordsEntity() { Word = uw });
            }

            await _db.BulkInsertAsync<WordsEntity>(entityList);

            return NoContent();
        }

        [Route("watchlistwords")]
        [HttpPost]
        public async Task<IActionResult> AddWatchlistWords([FromBody]List<string> words)
        {
            var lowercaseWords = words.Select(x => x.ToLower());
            var duplicates = _db.WatchList.Where(x => lowercaseWords.Contains(x.Word)).Select(x => x.Word).ToList();
            var entityList = new List<WatchlistWordsEntity>();

            foreach(var w in lowercaseWords)
            {
                if (!duplicates.Contains(w))
                {
                    entityList.Add(new WatchlistWordsEntity()
                    {
                        Word = w
                    });
                    duplicates.Add(w);
                }
            }

            if(entityList.Count > 0)
            {
                await _db.BulkInsertAsync<WatchlistWordsEntity>(entityList);
            }

            return NoContent();
        }

    }
}