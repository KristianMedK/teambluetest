﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TeamBlueTest.Entities
{
    [Table("Words")]
    public class WordsEntity : EntityBase
    {
        public string Word { get; set; }
    }
}
