﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TeamBlueTest.Entities
{
    [Table("WatchlistWords")]
    public class WatchlistWordsEntity : EntityBase
    {
        public string Word { get; set; }
    }
}
