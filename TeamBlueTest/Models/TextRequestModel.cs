﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TeamBlueTest.Models
{
    public class TextRequestModel
    {
        [JsonProperty("text")]
        public string Text { get; set; }
    }
}
