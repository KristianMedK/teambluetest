﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TeamBlueTest.Models
{
    public class TextResponseModel
    {

        [JsonProperty("distinctUniqueWords")]
        public int distinctUniqueWords { get; set; }

        [JsonProperty("watchlistWords")]
        public List<string> WatchlistWords { get; set; }
    }
}
