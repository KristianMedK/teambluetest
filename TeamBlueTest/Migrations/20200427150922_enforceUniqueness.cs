﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TeamBlueTest.Migrations
{
    public partial class enforceUniqueness : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Word",
                table: "Words",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Word",
                table: "WatchlistWords",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Words_Word",
                table: "Words",
                column: "Word",
                unique: true,
                filter: "[Word] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_WatchlistWords_Word",
                table: "WatchlistWords",
                column: "Word",
                unique: true,
                filter: "[Word] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Words_Word",
                table: "Words");

            migrationBuilder.DropIndex(
                name: "IX_WatchlistWords_Word",
                table: "WatchlistWords");

            migrationBuilder.AlterColumn<string>(
                name: "Word",
                table: "Words",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Word",
                table: "WatchlistWords",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
