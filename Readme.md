Mappen Test indeholder et par tekstfiler med curl commands til at teste api'et, samt et eksporteret insomnia workspace med samme kommandoer til test.

Der er brugt entity framework codefirst til at oprette databasen og tabeller. Connectionstring ligger i appsettings.
Der er tekstfiler med sql til create table/database, det er taget fra Microsoft SQL Server Management Studio ved højreklik og "script table/database as" -> createTo, men Migration filerne er mere læsbare imo. 